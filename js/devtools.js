chrome.devtools.panels.create("WebEngage", "gfx/devtools-logo.png", "panel.html", function (extensionPanel) {
	// var runOnce = false;
 //    extensionPanel.onShown.addListener(function(panelWindow) {
 //        if (runOnce) return;
 //        runOnce = true;
        
 //        //panelWindow.document.body.appendChild(document.createTextNode('Hello!'));
 //    });
});

// Create a connection to the background page
var backgroundPageConnection = chrome.runtime.connect({
    name: "devtools-page"
});

backgroundPageConnection.onMessage.addListener(function (message) {
    // Handle responses from the background page, if any
});

// Relay the tab ID to the background page
chrome.runtime.sendMessage({
    tabId: chrome.devtools.inspectedWindow.tabId,
    scriptToInject: "content_script.js"
});