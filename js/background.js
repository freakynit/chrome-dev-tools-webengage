/*
Allows to read, change and override settings kept in localStorage

FIXME Can be replaced with chrome.storage.local as soon as http://crbug.com/178618 will be resolved
FIXME Can be replaced with localStorage on the panel page as soon as http://crbug.com/319328 will be resolved
 */
chrome.runtime.onMessage.addListener(function(message, sender, callback){
	"use strict";

	if(message.name === 'getSettings') {
		callback(localStorage);
	} else if(message.name === 'setSettings') {
		localStorage = message.data;
	} else if(message.name === 'changeSetting') {
		localStorage[message.item] = message.value;
	}
});



// Background page -- background.js
chrome.runtime.onConnect.addListener(function(devToolsConnection) {
    var devToolsListener = function(message, sender, sendResponse) {
        // Inject a content script into the identified tab
        chrome.tabs.executeScript(message.tabId, {file: message.scriptToInject});
    }
    
    devToolsConnection.onMessage.addListener(devToolsListener);
    devToolsConnection.onDisconnect.addListener(function() {
         devToolsConnection.onMessage.removeListener(devToolsListener);
    });
});


// function onLoaded() {
// 	// for console interface
//   	chrome.experimental.devtools.console.onMessageAdded.addListener(function(msg) {
//     	console.log('console catched - ' + msg);
//   	});
// }

// window.onload = onLoaded;
