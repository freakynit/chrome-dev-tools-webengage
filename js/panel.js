function isArray(o) {
    return Object.prototype.toString.call(o) === '[object Array]';
}

function isFunction(o) {
    return Object.prototype.toString.call(o) === '[object Function]';
}

function getBrowserDetect(o){
	var BrowserDetectInfo = {};
	for(key in o) {
	    if(o.hasOwnProperty(key)) {
	        if(isFunction(o[key])) {
	            var val = o[key]();
	            BrowserDetectInfo[key] = val;
	        } else {
	            var val = o[key];
	            BrowserDetectInfo[key] = key;
	        }
	    }
	}

	return BrowserDetectInfo;
}

function buildHtmlTable(jsonData, $ele) {
	if (!isArray(jsonData)) {
        return ;
    }

    var $table = $('<table border="1" cellpadding="5" cellspacing="0"></table>');
    $ele.append($table);

	var columns = addAllColumnHeaders(jsonData, $table);

    for (var i = 0; i < jsonData.length; i++) {
        var row$ = $('<tr/>');
        for (var colIndex = 0; colIndex < columns.length; colIndex++) {
            var cellValue = jsonData[i][columns[colIndex]];

            if (cellValue == null) {
                cellValue = "";
            }

            row$.append($('<td/>').html(cellValue));
        }

        $table.append(row$);
    }
}

function addAllColumnHeaders(jsonData, $ele) {
    var columnSet = [];
    var headerTr$ = $('<tr/>');

    for (var i = 0; i < jsonData.length; i++) {
        var rowHash = jsonData[i];
        for (var key in rowHash) {
            if ($.inArray(key, columnSet) == -1) {
                columnSet.push(key);
                headerTr$.append($('<th/>').html(key));
            }
        }
    }
	
	$ele.append(headerTr$);

    return columnSet;
}


(function () {
	"use strict";
	
	var $weGeoContainer, $browserDetectContainer;

	$(document).ready(function(){
		$weGeoContainer = $("#webengage-geo-content");
		$browserDetectContainer = $("#webengage-browser_detect-content");
		
		fillWebEngageGeo();
		fillWebEngageBrowserDetect();
	});

	//initViews();
	//testCode();
	function fillWebEngageGeo(){
		chrome.devtools.inspectedWindow.eval(
          	// "jQuery.fn.jquery",
          	"webengage.GEO",
           	function(result, exceptionInfo) {
        	 	if (exceptionInfo) {
        	 		if(exceptionInfo.isError) {
        	 			var longDescription = "";
        	 			exceptionInfo.details.forEach(function(ele, idx){
        	 				longDescription += ele + "\n";
        	 			});

        	 			var errorMessage = "DevTools error. Code: " + exceptionInfo.code + ", description: " + exceptionInfo.description + "\n\n" + "Full description: " + longDescription;
        	 			alert(errorMessage);
        	 		} else if(exceptionInfo.isException) {
        	 			var errorMessage = "";
        	 			errorMessage += "Exception in evaluating code: " + exceptionInfo.value;
        	 			alert(errorMessage);
        	 		}
               	} else {
               		//alert("result: " + JSON.stringify(result) + ": " + typeof buildHtmlTable);
               		var jsonData = result;
               		if (!isArray(jsonData)) {
				        jsonData = [jsonData];
				    }

               		buildHtmlTable(jsonData, $weGeoContainer);
             	}
           	}
      	);
	}

	function fillWebEngageBrowserDetect(){
		chrome.devtools.inspectedWindow.eval(
          	// "jQuery.fn.jquery",
          	//"webengage.BrowserDetect.os",
          	"webengage.GEO"
           	function(result, exceptionInfo) {
        	 	if (exceptionInfo) {
        	 		if(exceptionInfo.isError) {
        	 			var longDescription = "";
        	 			exceptionInfo.details.forEach(function(ele, idx){
        	 				longDescription += ele + "\n";
        	 			});

        	 			var errorMessage = "DevTools error. Code: " + exceptionInfo.code + ", description: " + exceptionInfo.description + "\n\n" + "Full description: " + longDescription;
        	 			alert(errorMessage);
        	 		} else if(exceptionInfo.isException) {
        	 			var errorMessage = "";
        	 			errorMessage += "Exception in evaluating code: " + exceptionInfo.value;
        	 			alert(errorMessage);
        	 		}
               	} else {
               		var jsonData = getBrowserDetect(result);
               		if (!isArray(jsonData)) {
				        jsonData = [jsonData];
				    }

               		buildHtmlTable(jsonData, $browserDetectContainer);
             	}
           	}
      	);
	}
	

	function initViews(){
		console.log("initViews");
		
		var i = 0;
		$(document).ready(function(){
			console.log("jQuery ready");
	        var $m = $("#main-button-row");
	        $m.html("try: " + i++);
	        setInterval(function(){
	        	try {
	        		//$m.html(window.foo);
	        		$m.html("try: " + i++);
	        	} catch(err){
	        		console.log("err", err);
	        		$m.html("try: " + i++);
	        	}
	        }, 1000);
	    });
	}

	// function testCode(){
	// 	alert("testCode");
	// 	try {
	// 		chrome.tabs.query({"active":true,"currentWindow":true,"status":"complete","windowType":"normal"}, function(tabs) {
	// 			for(tab in tabs) {
	// 				chrome.tabs.sendMessage(tabs[tab].id, {text: "report_back"}, function(response) {
	// 		            alert("response = " + response);
	// 		        });
	// 			}
	// 	    });
	// 	} catch(err){
	// 		alert(err);
	// 	}
	// }

	function restoreSettings() {
		// Since we can't access localStorage from here, we need to ask background page to handle the settings.
		// Communication with background page is based on sendMessage/onMessage.
		chrome.runtime.sendMessage({
			name: 'getSettings'
		}, function (settings) {
			chrome.runtime.sendMessage({
				name: 'setSettings',
				data: settings
			});
		});

	}

	function persistSettingAndProcessSnapshot() {
		chrome.runtime.sendMessage({
			name: 'changeSetting',
			item: this.id,
			value: (this.type === 'checkbox') ? this.checked : this.value
		});
	}
})();
