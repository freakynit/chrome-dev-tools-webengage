function isArray(o) {
    return Object.prototype.toString.call(o) === '[object Array]';
}

function buildHtmlTable(jsonData, $ele) {
    var columns = addAllColumnHeaders(jsonData);
    if (!isArray(jsonData)) {
        jsonData = [jsonData];
    }

    console.log(isArray(jsonData));
    for (var i = 0; i < jsonData.length; i++) {
        var row$ = $('<tr/>');
        for (var colIndex = 0; colIndex < columns.length; colIndex++) {
            var cellValue = jsonData[i][columns[colIndex]];

            if (cellValue == null) {
                cellValue = "";
            }

            row$.append($('<td/>').html(cellValue));
        }

        $ele.append(row$);
    }
}

function addAllColumnHeaders(jsonData, $ele) {
    var columnSet = [];
    var headerTr$ = $('<tr/>');

    for (var i = 0; i < jsonData.length; i++) {
        var rowHash = jsonData[i];
        for (var key in rowHash) {
            if ($.inArray(key, columnSet) == -1) {
                columnSet.push(key);
                headerTr$.append($('<th/>').html(key));
            }
        }
    }
    $ele.append(headerTr$);

    return columnSet;
}
